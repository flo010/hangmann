import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
public class Hangmann {
    private static Scanner scanner = new Scanner(System.in);
    private static String input = "x";
    private static String Player1 = "x";
    private static String Player2 = "O";
    private Boolean GameState = true;
    private static String[][] Words = {{"H","e","l","l","o"}};
    private static String[] GameWord = {"_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_",}; //Max word length 20
    String x = "";


    public static void main(String args[]) {
        String Bye = "Thanks for plaing Hangmann";
        Hangmann Game= new Hangmann();

        do {
            int Word =Game.newWord();
            Game.GameLogic(Word);
            Game.GameState = newGame();
        }while (Game.GameState == true);
        System.out.println(Bye);
        return;


    }

    static boolean GameInput( int Word) {
        boolean Right = false;
        System.out.println("Guess a Letter by writing it");
        String Input = readInput();
        for(int i=0;i<Words[Word].length;i++)
        {
            if(Words[Word][i]==Input){
                Right = true;
                GameWord[i] = Input;
            }
        }
        return Right;
    }

    void ConsoleOutput(int Word) {

        System.out.println("The Current PLayingstate:");
        System.out.println("");
        System.out.println("-------------------------------------------");
        for(int i=0;i<Words[Word].length;i++) {
            System.out.println(GameWord[i]);
        }
        System.out.println("-------------------------------------------");
        return;
    }

    static String readInput() {
        input = scanner.nextLine();
        return input;
    }



    void GameLogic(int Word){
        String GameWon = "";
        int rounds = 10;
        Boolean loop = true;
        do {
            ConsoleOutput(Word);
            if(GameInput(Word) == false);{rounds= rounds-1;}
            if (Win(Word) == true) {
                loop = false;
                GameWon= "Game was won by the Players in " + rounds +"rounds.";

            }
            if (rounds==0)
            {
                loop = false;
                GameWon= "Game was lost by the Players";
            }
        }
        while(loop == true);
        System.out.println(GameWon);
        return;
    }



    boolean Win(int Word) {
        boolean x = true;
        for(int i=0;i<Words[Word].length;i++)
        {
            if(Words[Word][i]!=GameWord[i]){x = false;}
        }
        return x;
    }

    static boolean newGame(){
        Boolean Answer = true;
        Boolean loop = false;
        System.out.println("Do o want to start a new game? If yes type 'y', else type 'n'.");
        do {
            loop = false;
            if (readInput() == "y") {
                return true;

            } else if (readInput() == "n") {
                return false;
            }
            else
            {
                System.out.println(" Type 'y' or 'n'.");
                loop=true;
            }
        }while(loop==true);
        return true;
    }
    int newWord()
    {
        int randomint = ThreadLocalRandom.current().nextInt(Words.length);
        return randomint;
    }


}


